﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAll : MonoBehaviour {

    GameObject player;
    public float distanse = 550f;
    Transform destroyTrans;
    Transform playerTrans;

    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject.transform.parent.gameObject;
        Destroy(obj);
    }


    // Use this for initialization
    void Start () {
        destroyTrans = this.transform;
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.GetComponent<Transform>();

	}
	
	// Update is called once per frame
	void Update () {
        float dist = Vector3.Distance(transform.position, playerTrans.position);
        
        if (dist > distanse)
        {
            destroyTrans.position = new Vector3(destroyTrans.position.x - 0.5f, 0, 0);
        }
       
        //  float distansceX = playerTrans.position.x + distanse;
    //    destroyTrans.position = new Vector3(distansceX, 0, 0);
      
        
	}
}
