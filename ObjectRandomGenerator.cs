﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class ObjectRandomGenerator : MonoBehaviour {

    [Header("Заполнить объектами с ТЕГАМИ 1х1 1х2 " + "итд порядок значения не имеет")]
    [Header("Если обЪект создается с хуевой высотой-проверить ось Y " + "должно быть 45")]
 
    public UnityEngine.Object[] RandomObjects;
    [Header(" ")]
    [Header("Шанс появления объекта в этой точке в процентах")]
    public float ChanseToCreate = 100f;

   
    /// <summary>
    /// Рисуется гизма, больше в не ничего не делать, не дает вызывать дурие методы((
    /// </summary>
    void OnDrawGizmos()
    {
       
        switch (gameObject.name)
        {
            case "1x1-random-object":
                Gizmos.color = new Color(1, 0, 0, 0.5F);
                Gizmos.DrawCube(transform.position, new Vector3(1, 2, 1));

                break;
            case "1x2-random-object":
                Gizmos.color = new Color(1, 0, 0, 0.5F);
                Gizmos.DrawCube(transform.position, new Vector3(1, 2, 2));

                break;
            case "2x2-random-object":
                Gizmos.color = new Color(1, 0, 0, 0.5F);
                Gizmos.DrawCube(transform.position, new Vector3(2, 2, 2));

                break;
            case "3x2-random-object":
                Gizmos.color = new Color(1, 0, 0, 0.5F);
                Gizmos.DrawCube(transform.position, new Vector3(3, 2, 2));

                break;

            default:
              
                break;
        }
    }

 
    /// <summary>
    /// берет имя куба с гизмой, если имя совпадает с тегом объекта из массива с объектами то добоавляет их в лист и потом рандомно создает в месте гизма Куба (чуть выше). Потом кидает рейкаст вниз и опускает объект на землю.  
    /// </summary>
    public void  CreateObject()
    {
        if (ChanseToCreate> UnityEngine.Random.Range(0,100))
        {

        
        List<GameObject> temObj = new List<GameObject>();
        foreach (GameObject obj in RandomObjects)
            {
                if (gameObject.name == obj.tag)
                    {
                      temObj.Add(obj);
                    }
            }
            GameObject toCreate = temObj[UnityEngine.Random.Range(0, temObj.Count)];
           // Quaternion qa= new Quaternion();
            GameObject CreatedObj = Instantiate(toCreate , transform.position + (Vector3.down /1.3f), toCreate.transform.rotation);
            CreatedObj.transform.parent = gameObject.transform;
            RaycastHit hit;
            
            if(Physics.Raycast(CreatedObj.transform.position, CreatedObj.transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
            {
                Debug.DrawRay(CreatedObj.transform.position, CreatedObj.transform.TransformDirection(Vector3.down) * hit.distance, Color.green);
                CreatedObj.transform.position -= Vector3.down * Time.deltaTime;
            }
        }

    }

    private void Awake()
    {
     

    }


    void Start () {

        CreateObject();
    }
	
	void Update () {
		
	}
}
