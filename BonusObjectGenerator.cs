﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObjectGenerator : MonoBehaviour {

   
    Vector3 castPosition = new Vector3();
    public float chanceToCast = 3;
    public float x = 10f;
    public float z =35f;
    GameObject obj;
    public GameObject[] BonusObjects;
    bool canCast = true;
    RaycastHit hit;

void NewPosition()
    {
      castPosition =  this.gameObject.transform.position + new Vector3(Random.Range(-x, x), 3f, Random.Range(-z, z));
    }

    void CastObject()
    {
        int i = Random.Range(0, BonusObjects.Length);
       obj = Instantiate(BonusObjects[i],  castPosition, Quaternion.identity);
        obj.transform.parent = gameObject.transform;
    }



    private void Awake()
    {
         // BonusObjects = GameObject.FindGameObjectsWithTag("bonus");

    }
  

// Use this for initialization
void Start ()
   {
        float c = Random.RandomRange(0, 100);
        if (canCast && chanceToCast < c)
        { 
        NewPosition();
        CastObject();
        canCast = false;
        }

       

   }

     


// Update is called once per frame
void Update () {

        if(obj != null) {

            if (Physics.Raycast(obj.transform.position, obj.transform.TransformDirection(Vector3.down), out hit, 5f))
            {
                Debug.DrawRay(obj.transform.position, obj.transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
              
            }
            else
            {
                NewPosition();
                obj.transform.position = castPosition;
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 1000, Color.white);
              
            }

        }
       
    }

    private void OnCollisionEnter(Collision collision)
    {
      //  if(collision.other)
    }

   
}
