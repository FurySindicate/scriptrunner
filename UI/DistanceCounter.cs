﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceCounter : MonoBehaviour {

    Transform origin;
    float RoundDistance;
    Text counterText;
    TextMesh BestDistanceText;
    public GameObject BestDistanceOBJ;
    GameObject startPoint;
    GameObject player;
    float bestDistance;

    
    void Update()
    {
        //Calculate distance 
        RoundDistance = Vector3.Distance(origin.position, transform.position);
        Managers.player.SetRoundDistance(RoundDistance);
        counterText.text = "Distance: " + Managers.player.RoundDistance.ToString();
        if (bestDistance > 10f)
        {
            BestDistanceText.text = "Best Distance " + bestDistance.ToString();
        }else
        {
            BestDistanceText.text = " ";
        }
    }

    void Start()
    {
        bestDistance = PlayerPrefs.GetFloat("Best Distance", 0);
        startPoint = GameObject.Find("StartPoint");
        origin = startPoint.transform;
        counterText = GameObject.Find("DistanceCounterText").GetComponent<Text>();
        BestDistanceOBJ = GameObject.Find("BestDistanceText");   
        player = GameObject.FindGameObjectWithTag("Player");
        BestDistanceOBJ.transform.position = new Vector3(0, 1f, player.transform.position.z);
        BestDistanceText = BestDistanceOBJ.GetComponent<TextMesh>();
    }
    private void Awake()
    {
        counterText = GameObject.Find("DistanceCounterText").GetComponent<Text>();
    }
    private void LateUpdate()
    {
        if (bestDistance > 10f)
        {
            BestDistanceOBJ.transform.position = new Vector3(-bestDistance, 1f, player.transform.position.z);
            BestDistanceOBJ.SetActive(true);
        }
    }
}
