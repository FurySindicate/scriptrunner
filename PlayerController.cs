﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float jump;
    public float jumpPower; // мощность прыжка
    public float turnSpeed = 50f;
  
    public int DecreaseStaminaCount = 10;

    public Text moneyText;
    

    private float gravityForce; // гравитация
    private Vector3 moveVector; // Направление движения игрок
    private CharacterController playerContr;

    Animator anim;


    // Use this for initialization
    void Start()
    {
        moneyText = GameObject.Find("moneyText").GetComponent<Text>();
        anim = GetComponent<Animator>();
        playerContr = GetComponent<CharacterController>();
    
    }

    private void FixedUpdate()
    {
        AnimPlayer(); // //анимация игрока
        PlayerMove(); // перемещение игрока
        Jumping(); // прыжок
        Tricks(); // трюки
     //   BoostTimeSlider.value = boostTime;
   
    }

    private void Update()
    {
        moneyText.text = " Money: " + Managers.player.money.ToString();
    }


    private void AnimPlayer() //анимация игрока
    {
        anim.SetFloat("Speed", moveVector.x, 0.1f, Time.deltaTime);
       // anim.SetFloat("Direction", moveVector.z, 0.1f, Time.deltaTime);
        anim.SetFloat("Jump", jump);
        anim.SetBool("Trick1", true);
        anim.SetBool("Trick2", true);
        anim.SetBool("Trick3", true);
    }



    private void PlayerMove() //условия перемещения игрока
    {
        if (Managers.player.isAlive)
        { 
        moveVector = Vector3.zero;
        moveVector.x = Input.GetAxis("Vertical") /** (Managers.player.Playerspeed)*/;
     //   moveVector.z = Input.GetAxis("Horizontal") * Managers.player.turnSpeed;


        if (Input.GetKey(KeyCode.LeftShift) && Managers.player.boostTime > 0)
        {    
            Managers.player.DecreaseStamina(DecreaseStaminaCount);  // состояние перехода задается в DecreaseStamina в PlayerManager
            }
            else
            {
                Managers.player.StaminaReproduce();   
            }

                    if (Input.GetKey(KeyCode.LeftArrow))
                    {
                       transform.Rotate(Vector3.up, -Managers.player.turnSpeed * Time.deltaTime);
                    }

                        if (Input.GetKey(KeyCode.RightArrow))
                        {
                         transform.Rotate(Vector3.up, Managers.player.turnSpeed * Time.deltaTime);
                        }
               

          playerContr.Move(moveVector * Time.deltaTime);
        }
    }

    
    public void Jumping() // прыжок
    {
        if (Input.GetKey(KeyCode.Space))
        {
            jump = 0.2f;
        }
        else
        {
            jump = 0.0f;
        }
    }


    


    public void Tricks() // трюки
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            anim.SetBool("Trick1", true);
        }
        else
        {
            anim.SetBool("Trick1", false);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            anim.SetBool("Trick2", true);
        }
        else
        {
            anim.SetBool("Trick2", false);
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            anim.SetBool("Trick3", true);
        }
        else
        {
            anim.SetBool("Trick3", false);
        }
    }
}