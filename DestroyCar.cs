﻿using UnityEngine;
using System.Collections;

public class DestroyCar : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collider Enter");
        if (other.gameObject.CompareTag("car")) Destroy(other.gameObject);
    }

    void OnCollisionEnter(Collision otherc)
    {
       // Debug.Log("Collision Enter");
        if (otherc.gameObject.CompareTag("car")) Destroy(otherc.gameObject);
    }

}
