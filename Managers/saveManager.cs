﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class saveManager : MonoBehaviour, IGameManager
{

    public ManagerStatus status { get; private set; }

  public string[] perfFloatName = new string[3] { "Best Distance", "Total Distance", "Money" };
  public string[] perfSkilsStaminaName = new string[3] { "StaminaSprint", "StaminaRestoring", "StaminaCount" };
  public string[] perfSkilsTimeName = new string[3] { "TimeRestoring", "TimeRestoring", "TimeCount" };


    ///public float Playerspeed { get; private set; }

    public void Startup()
    {
        Debug.Log("Save Manager starting...");
        CheckNullState(perfFloatName);
        CheckNullState(perfSkilsStaminaName);
        CheckNullState(perfSkilsTimeName);
     

        status = ManagerStatus.Started;
    }

    /// <summary>
    /// ВОзвращает значение из сохранений
    /// 0 Best Distance , 1 Total Distance
    /// </summary>
    /// <param name="index"> значение из массива с именами плеер перфов</param>
  
   public void SaveAll()
    {
       if(PlayerPrefs.GetFloat(perfFloatName[0]) < Managers.player.RoundDistance)  PlayerPrefs.SetFloat(perfFloatName[0], Managers.player.RoundDistance);
        PlayerPrefs.SetFloat(perfFloatName[1], PlayerPrefs.GetFloat(perfFloatName[0]) + Managers.player.RoundDistance) ;
        PlayerPrefs.SetFloat(perfFloatName[2], Managers.player.money);
        //  PlayerPrefs.SetFloat(perf, 0);
        //  PlayerPrefs.SetFloat(perf, 0);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Удаляет все сейвы
    /// </summary>
    public void DeleteAllSaves()
    {
        PlayerPrefs.DeleteAll();
    }
    /// <summary>
    /// проверяет наличие сохранения, если его нет, то приравнивает к 0 
    /// </summary>
    public void CheckNullState(string[] _perfs)
    {
        foreach (string perf in _perfs)
        {
            if (!PlayerPrefs.HasKey(perf))
            {
                PlayerPrefs.SetFloat(perf, 0);
                Debug.Log(perf + " Was NULL");
                PlayerPrefs.Save();
            }
        }

    }

        // Use this for initialization
        void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
