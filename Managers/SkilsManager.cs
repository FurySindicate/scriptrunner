﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SkilsManager :  MonoBehaviour , IGameManager {


    public ManagerStatus status { get; private set; }
    /// <summary>
    /// уровень прокачки скила ускорения
    /// </summary>
    public float StaminaSprintLevel { get; private set; }
    /// <summary>
    ///  насколько увеличить скорость (зависит от уровня)
    /// </summary>
    public float StaminaSprintCount { get; private set; }
    /// <summary>
    /// Как бытро восстанавнивается
    /// </summary>
    public float StaminaSprintReproduce { get; private set; }
    /// <summary>
    /// Уровень прокачки
    /// </summary>
    public float StaminaSprintReproduceLevel { get; private set; }
    /*  public float StaminaRestoring { get; private set; }
      public float StaminaCount { get; private set; }*/
      /// <summary>
      /// 
      /// </summary>
    public float TimeWarpLevel { get; private set;}
    /// <summary>
    /// 
    /// </summary>
    public float TimeWarpSlowingPower { get; private set; }

    public enum skills
    {
        stamina,
        ReproduceSpeed,
        SprintSpeed,
        TimeWarp

    }
    public void Startup()
    {
        Debug.Log("Save Manager starting...");
        StaminaSprintLevel = PlayerPrefs.GetFloat("StaminaSprintLevel", 0);
        StaminaSprintReproduceLevel = PlayerPrefs.GetFloat("StaminaSprintReproduceLevel", 0);
        TimeWarpLevel = PlayerPrefs.GetFloat("TimeWarpLevel", 0);

        // проверить эти 2 строки
        StaminaSprintCount = PlayerPrefs.GetFloat("StaminaSprintLevel", 0);
        TimeWarpSlowingPower = PlayerPrefs.GetFloat("TimeWarpLevel", 0);

        StaminaSprintReproduceLevel = PlayerPrefs.GetFloat("StaminaSprintReproduceLevel", 0);

        // тут начальные значения для нулевого уровня
        StaminaSprintCount = StaminaSprintLevel * 0.1f;
        if (StaminaSprintReproduceLevel == 0) StaminaSprintReproduceLevel = 0.3f;
         StaminaSprintReproduce = StaminaSprintReproduceLevel * 0.1f;

        status = ManagerStatus.Started;
    }
    /// <summary>
    /// добавить уровень для скила ускорения скорости
    /// </summary>
    public void AddStaminaSpeedLevel()
    {
        StaminaSprintLevel = StaminaSprintLevel + 1;
        PlayerPrefs.SetFloat("StaminaSprintLevel", StaminaSprintLevel);
        StaminaSprintCount = StaminaSprintLevel * 0.1f;
    }

    public void AddStaminaSprintReproduceLevel()
    {
        StaminaSprintReproduceLevel = StaminaSprintReproduceLevel + 1;
        PlayerPrefs.SetFloat("StaminaSprintReproduceLevel", StaminaSprintReproduceLevel);
        StaminaSprintReproduce = StaminaSprintReproduceLevel * 0.1f;
    }



    public void BuyLevel(skills _skill)
    {
        switch (_skill)
        {
            case skills.stamina:
                {
                    print("Hello and good day!");
                   
                }
                break;
            case skills.SprintSpeed:
                {
                    if (CheckMoney(StaminaSprintLevel))
                    {
                        AddStaminaSpeedLevel();
                        PlayerPrefs.SetFloat("StaminaSpeed", StaminaSprintLevel);
                        print(" YOUR STAMINA LEVE IS " + StaminaSprintLevel.ToString() + "\n\r");
                    }

                
                }
                break;
            case skills.ReproduceSpeed:
                if(CheckMoney(StaminaSprintReproduceLevel))
                {
                    AddStaminaSprintReproduceLevel();
                    PlayerPrefs.SetFloat("StaminaSprintReproduceLevel", StaminaSprintReproduceLevel);
                    print(" YOUR  Sprint Reproduce Level IS " + StaminaSprintReproduceLevel.ToString() + "\n\r");
                }


                break;
            default:
                print("Incorrect intelligence level.");
                break;
        }
    }

    public float GetPrice(float level)
    {
        float price = (level + 10f) * 10f;
        return price;
    }

    /// <summary>
    /// проверяет достаточно ли денег для покупки уровня
    /// </summary>
    /// <param name="SkillLevel">уровень скила</param>
    /// <returns></returns>
    public bool CheckMoney(float SkillLevel)
    {
        float price = GetPrice(SkillLevel);
        if (Managers.player.money >= price)
        {
            print("ADD LEVEL");
            Managers.player.LeaveMoney(price);
            PlayerPrefs.SetFloat("Money", Managers.player.money);
            return true;
        }
        else
        print("NOT ENOTH MONEY. YOU NEED " + GetPrice(SkillLevel).ToString() + "$ \n\r" + " YOUR STAMINA LEVE IS " + SkillLevel.ToString() + "\n\r");
        return false;
    }
   /*
    public void skill()
    {

    }

    public void skill()
    {

    }

    public void skill()
    {

    }
    */



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(1)) BuyLevel(skills.SprintSpeed);
        if (Input.GetMouseButtonDown(2)) Managers.player.AddMoney(1000);
        if (Input.GetKeyDown("1")) BuyLevel(skills.ReproduceSpeed);
        if (Input.GetKeyDown("0")) PlayerPrefs.DeleteAll();
    }
}
