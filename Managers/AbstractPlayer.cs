﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractPlayer {

    public float speed = 5f;
    public float turnSpeed = 5f;
    public bool isAlive = true;

    public virtual void Dead()
    {
        isAlive = false; 
    }
}
