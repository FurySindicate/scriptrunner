﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(PlayerManager))]
[RequireComponent(typeof(saveManager))]
[RequireComponent(typeof(SkilsManager))]
///  НЕ ЗАБЫВАТЬ ЭТУ ХЕРЬ СВЕРХУ

public class Managers : MonoBehaviour {

    public static PlayerManager player { get; private set; }
    public static saveManager saveLoad { get; private set; }
    public static SkilsManager skils { get; private set; }

    private List<IGameManager> _startSequence;


    private void Awake()
    {
        player = GetComponent<PlayerManager>();
        saveLoad = GetComponent<saveManager>();
        skils = GetComponent<SkilsManager>();

        _startSequence = new List<IGameManager>();
        _startSequence.Add(player);
        _startSequence.Add(saveLoad);
        _startSequence.Add(skils);
        StartCoroutine(StartupManagers());
    }

    private IEnumerator StartupManagers()
    {
        foreach (IGameManager manager in _startSequence)
        {
            manager.Startup();
            
        }
        yield return null;

        int numModules = _startSequence.Count;
        int numReady = 0;

        while (numReady < numModules)
        {
            int lastReady = numReady;
            numReady = 0;

            foreach (IGameManager managers in _startSequence)
            {
                if (managers.status == ManagerStatus.Started)
                {
                    numReady++;
                }
            }
            if (numReady > lastReady)
            {
                Debug.Log("Progress: " + numReady + "/" + numModules);
                yield return null;
            }
        }
            Debug.Log("All Managers started up");
        
    }
    
}
