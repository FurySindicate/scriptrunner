﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerManager : MonoBehaviour, IGameManager {

    public ManagerStatus status { get; private set; }

    public float Playerspeed { get; private set; }  //это состояние аниматора
    public float turnSpeed { get; private set; }
   

    public bool isAlive { get; private set; }
    public float boostTime { get; private set; }
    public float money { get; private set; }

    public float RoundDistance { get; private set; }
    public float TotalDistance { get; private set; }

    public GameObject Player { get; private set; }
    public Animator animator { get; private set; }


    Slider BoostTimeSlider;
 


    public void Startup()
    {
        money = PlayerPrefs.GetFloat("Money",0);
        Debug.Log("Player Manager starting...");
        RoundDistance = 0;
        Player =  GameObject.FindGameObjectWithTag("Player");
        animator = Managers.player.Player.GetComponent<Animator>();
        isAlive = true;
        boostTime = 100f;
        turnSpeed = 100f; // можно загружать из плеер перфс
        

        BoostTimeSlider = GameObject.Find("BoostTimeSlider").GetComponent<Slider>();
        BoostTimeSlider.value = boostTime;


        status = ManagerStatus.Started;
    }
    #region Всякие собирательские бонусы
    /// <summary>
    /// Добавляет денег
    /// </summary>
    /// <param name="_money">сколько добавить</param>
    public void AddMoney(int _money)
    {
        money += _money;
        Debug.Log("added " + _money.ToString());
    }

    public void LeaveMoney(float _money)
    {
        money -= _money;
        Debug.Log("Leave money = - " + _money.ToString());
    }

    /// <summary>
    /// Добавить выносливости
    /// </summary>
    /// <param name="_boost">сколько добавить</param>
    public void AddBost(float _boost)
    {
        boostTime += _boost;
        Debug.Log("added " + _boost.ToString());
    }
    #endregion

    #region параметры игрока
    /// <summary>
    /// Уменьшает выносливость
    /// </summary>
    /// <param name="_count">множитель уменьшения int</param>

    public void DecreaseStamina(int _count)
    {
        animator.speed = 1.5f + Managers.skils.StaminaSprintCount;
       // turnSpeed = turnSpeed + animator.speed;                        ///////тут придумать как увеличивать скорость поворота приускорении
        boostTime = boostTime - Time.deltaTime * _count;
    }

    /// <summary>
    /// восстановление выносливости
    /// </summary>
    public void StaminaReproduce()
    {
        animator.speed = 1f;  //сюда доавить прокачку обычной скорости бега
        turnSpeed = 100f;  //сюда доавить прокачку обычной скорости поворота

        if (boostTime < 100)
        {
           // Debug.Log(Managers.skils.StaminaSprintReproduce);
            boostTime = boostTime + (Time.deltaTime * Managers.skils.StaminaSprintReproduce);
        }
    }

    public void KillPlayer()
    {
        Managers.saveLoad.SaveAll();
        isAlive = false;
        Player.GetComponent<Animator>().enabled = false;
    }
    #endregion

    public void SetRoundDistance(float dist)
    {
        RoundDistance = dist;
    }




    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        BoostTimeSlider.value = boostTime;

       
    }
}
