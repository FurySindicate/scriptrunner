﻿using UnityEngine;
using System.Collections;

public class CarMove : MonoBehaviour
{
    public float speed = 0.1f;
    public float speedA = 3f;
    public float speedB = 15f;
    public float maxSpeed = 15f;
    public float minSpeed = 5f;
    //public Rigidbody rb;
    //Use this for initialization

    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.CompareTag("Player")) Managers.player.KillPlayer();  // obj.GetComponent<Animator>().enabled = false;
    }

    void Start()
    {
        //	rb = gameObject.GetComponent<Rigidbody> ();
    }

    // Update is called once per frame
    void Update()
    {
        speed = Mathf.Clamp(speed + Random.Range(speedA, speedB) * Time.deltaTime / 2f, minSpeed, maxSpeed);

        transform.Translate(Vector3.forward * speed);
        //rb.AddForce(transform.forward * Time.deltaTime * speed*10);
    }
}
