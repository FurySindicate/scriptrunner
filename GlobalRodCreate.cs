﻿using UnityEngine;
using System.Collections;
/// <summary>
/// скрипт с глобальными переменными и функцией создания зон. Функция инициализируется в ZoneDetector
/// </summary>

public class GlobalRodCreate : MonoBehaviour {

    public GameObject FirstZone;
    public GameObject[]Zona ;   // 10 зон, нулевая зона стартовая ее скрипт не трогает(только удаляет)
    GameObject player;
    GameObject ZoneTrigger;
    public float CreateDistance = 15f;
    //public Transform offset = new Vector3(25, 0, 0);
    Vector3 transFZ;

    private void Update()
    {
        
        if (Input.GetMouseButtonUp(0)) { CreateZone(); }
        float _distance = Vector3.Distance(player.transform.position, ZoneTrigger.transform.position);
        if (_distance < CreateDistance)
        {
            CreateZone();
            ZoneTrigger.transform.position += new Vector3(-15, 0, 0);
        }

    }
//
	
    private void Start()
    {
        ZoneTrigger = GameObject.FindGameObjectWithTag("ZoneTrigger");
        player = GameObject.FindGameObjectWithTag("Player");

        GameObject NewZone = Instantiate(FirstZone);//(Zona[Random.Range(0, Zona.Length)]);
        Transform transFZ = FirstZone.transform;
    }

    public void CreateZone()
    {
        GameObject NewZone;
        transFZ = new Vector3(FirstZone.transform.position.x - 29.8f, 0, 0);
        Quaternion rotation = FirstZone.transform.rotation;
        NewZone = Instantiate(Zona[Random.Range(0, Zona.Length)], transFZ, rotation);
        /// чтобы убрать проверку на одинаковые зоны удаляем while 
            while (GameObject.ReferenceEquals(FirstZone, NewZone))  // проеряет одинаковые ли исходники у создаваемых объектов 
            {
                destroy(FirstZone);
                NewZone = Instantiate(Zona[Random.Range(0, Zona.Length)], transFZ, rotation);
            }
        FirstZone = NewZone;
    }


}
